#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse
from numpy import mean, std

"""Swastik Mishra 2017 (swastikm@protonmail.com)"""

'''Pdb Parser for converting into groups'''

'''
“Things need not have happened to be true.
Tales and dreams are the shadow-truths that will endure when mere facts are dust and ashes, and forgot.”
― Neil Gaiman, Dream Country
'''

# list to check if any residue encountered is one of the 19
# residues, out of the 20 but excluding Proline
NoProList = ["ARG", "HIS", "LYS", "ASP", "GLU", "SER", "THR", "ASN",
             "GLN", "CYS", "ALA", "VAL", "ILE", "LEU", "MET", "PHE", "TYR", "TRP", "GLY"]

# - {'RESNAME': {('ATOMA', 'ATOMB1', 'ATOMB2')): 'CHEM_GROUP_NAME'}}
chemgroupdict = {
        'ALA': {0: ('CB', 'r8')},
        'SER': {0: (('CB', 'OG'), 'r7')},
        'THR': {0: (('CB', 'OG1'), 'r7'),
                1:('CG2', 'r8')
                    },
        'CYS': {0: (('CB', 'SG'), 'r10')},
        'VAL': {0: ('CB', 'r12'),
                1: ('CG1', 'r8'),
                2: ('CG2', 'r8')
                    },
        'LEU': {0: ('CB', 'r2'),
                1: ('CG', 'r12'),
                2: ('CD1', 'r8'),
                3: ('CD2', 'r8')
                    },
        'ILE': {0: ('CB', 'r12'),
                1: ('CG1', 'r2'),
                2: ('CG2', 'r8'),
                3: ('CD1', 'r8'),
                    },
        'MET': {0: ('CB', 'r2'),
                1: ((('CG', 'SD', 'CE'), ('CG', 'S', 'CE')), 'r13')
                    },
        'PRO': {0: ('relevant', 'r11')},
        'PHE': {0: ('CB', 'r2'),
                1: (('CG', 'CD1', 'CD2', 'CE1', 'CE2', 'CZ'), 'r14')
                    },
        'TYR': {0: ('CB', 'r2'),
                1: (('CG', 'CD1', 'CD2', 'CE1', 'CE2', 'CZ', 'OH'), 'r16'),
                    },
        'TRP': {0: ('CB', 'r2'),
                1: (('CG', 'CD1', 'CD2', 'NE1', 'CE2', 'CE3', 'CZ2', 'CZ3', 'CH2'), 'r15')
                    },
        'ASP': {0: (('CB', 'CG', 'OD1', 'OD2'), 'r6')},
        'GLU': {0: ('CB', 'r2'),
                1: (('CG', 'CD', 'OE1', 'OE2'), 'r6')
                    },
        'ASN': {0: ('CB', 'r2'),
                1: (('CG', 'OD1', 'ND2'), 'r9')
                    },
        'GLN': {0: ('CB', 'r2'),
                1: ('CG', 'r2'),
                2: (('CD', 'OE1', 'NE2'), 'r9')
                    },
        'HIS': {0: (('CB', 'CG', 'ND1', 'CD2', 'NE2', 'CE1'), 'r4')},
        'LYS': {0: ('CB', 'r2'),
                1: ('CG', 'r2'),
                2: ('CD', 'r2'),
                3: (('CE', 'NZ'), 'r5')
                    },
        'ARG': {0: ('CB', 'r2'),
                1: ('CG', 'r2'),
                2: (('CD', 'NE', 'CZ', 'NH1', 'NH2'), 'r3')
                    }
                 }

knownbfac_calculations = ['atomids', 'mean', 'perc', 'zscore', 'bzs', 'None']

def getFileNameWithoutExtension(path):
    # this function does exactly what it's name says (duh!)
    return path.split('\\').pop().split('/').pop().rsplit('.', 1)[0]

def getFileNameWithExtension(path):
    # this function does exactly what it's name says (duh!)
    return path.split('\\').pop().split('/').pop()

def thereader(readf_data, pdbfilename, sdir='./', ddir='./'):
    '''this function reads the pdbfile and returns
    readf_data
    readf_data is of the form:
        (atom_no, atom_name, res_name, chain, resno_column, x, y, z, bfac, cut)
    cut is such that it contains the atom_number-1
        for each atom where a new residue starts '''
    # sdir and ddir are directories
    sdir = sdir + '/'
    ddir = ddir + '/'

    pathplus_filename = sdir + pdbfilename
    # print(pathplus_filename)
    with open(pathplus_filename, 'r') as file1:
        f1 = file1.readlines()

    ano = []
    aid = []
    res = []
    chain = []
    resno = []
    x = []
    y = []
    z = []
    bfac = []
    # check if file has multiple models
    try:
        for line in f1:
            if line[0:6].strip() == "MODEL":
                print('MODEL statement found in ' + str(pdbfilename) +
                      " file. Possibly has multiple models. Aborting...")
                sys.exit(1)
    except Exception as model:
        print(str(model))
        print('Exception while checking for existence of multiple models in the pdb file' +
              pdbfilename + "\nAborting...")
        sys.exit(1)
    # read file
    try:
        for line in f1:
            if line.startswith("ATOM") and line[16] in ('A', ' '):
                # fname.append(pdbfilename[0:6])
                ano.append(int(line[6:11].strip()))
                aid.append(str(line[12:16].strip()))
                res.append(str(line[17:20].strip()))
                if line[21] == ' ':
                    chain.append('Z')
                else:
                    chain.append(line[21])
                resno.append(str(line[22:27]))
                x.append(float(line[30:38].strip()))
                y.append(float(line[38:46].strip()))
                z.append(float(line[46:54].strip()))
                bfac.append(float(line[60:66].strip()))
    except Exception as e:
        print('Exception occurred while reading the file: ' +
              str(pdbfilename) + '\nException is: '+str(e) +"Aborting...")
        sys.exit(1)

    cut = []
    cut.append(0)
    flag = resno[0]
    for i, j in enumerate(resno):
        try:
            if j != flag:
                cut.append(i)
                flag = j
        except Exception as f:
            print('Exception occurred in while reading residues in ' +
                  str(pdbfilename) + '\nException is: ' + str(f) + "Aborting...")
            sys.exit(1)
    cut.append(len(resno)-1)
    # print(bfac)
    readf_data = [ano, aid, res, chain, resno, x, y, z, bfac, cut]
    return readf_data

def bfac_col(bfacstuff, ribfac, aidrange, readf_data, chmgroup='r1'):
    from scipy.stats import percentileofscore
    '''if bfactoggle is:
        depth = calculate average ribfac, which is a list of values in bfac column for chemical
            group being defined in this iteration
        bfactor = calculate percentile bfactor based on current methodology
    '''
    rbfac = None                  # default rbfac value that it returns
    bfactoggle, bfacdict, exposedbfac, buriedbfac, bfaclist, dcutoff = bfacstuff
    if bfactoggle == 'mean':
        rbfac = mean(ribfac)
    elif bfactoggle == 'perc':
        rbfac = mean([percentileofscore(bfaclist, smx) for smx in ribfac])/100
    elif bfactoggle == 'zscore':
        rbfac = (mean([(x-mean(bfaclist))/std(bfaclist) for x in ribfac]))
    elif bfactoggle == 'bzs':
        bfaczscorelist = []
        for x in ribfac:
            if bfacdict[x] < dcutoff:
                bfaczscorelist.append(abs((x-mean(exposedbfac))/std(exposedbfac)))
                # print(str(bfacdict[x]) + ' was depth and x was ' + str(x))
            else:
                bfaczscorelist.append(abs((x-mean(buriedbfac))/std(buriedbfac)))
                # print(str(bfacdict[x]) + ' was depth and x was ' + str(x))
        rbfac = mean(bfaczscorelist)
    elif bfactoggle == 'atomids':
        if sorted(ribfac) != list(range(min(ribfac), max(ribfac)+1)) and chmgroup != 'r1':
            print("WARNING: atom numbers in one of the chemical groups are not continuous! Atom nos. list was: "+','.join(map(str, ribfac)))
        return ribfac
    # print(rbfac)

    return rbfac

def sidechainparser(bfacstuff, rc, i, aidrange, toparse, coordinates, readf_data, outputrecordsstring):
    '''parses side chain
    input are:
        rc = chemical group serial number in the to-be-written gpdb file
        aidrange = range of indices in aid that define a single residue
        toparse = is of the form ('RESNAME', ('ATOMA1','ATOMB2', 'ATOMB3'...))
        bfactoggle = one of the ways to calculate values to put in bfac column, as defined in
        knownbfac_calculations
    '''
    resname = toparse[0]
    groupnumber = toparse[1]
    start, end = aidrange
    x, y, z, aid = coordinates
    aid, res, chain, resno, x, y, z, bfac = readf_data[1:-1]
    # print(resname, rc)
    atomnameslist = chemgroupdict[resname][groupnumber][0]
    if resname == "MET" and groupnumber == 1:
        if 'SD' in aid[start:end]:
            atomnameslist = atomnameslist[0]
        elif 'S' in aid[start:end]:
            atomnameslist = atomnameslist[1]
    if isinstance(atomnameslist, str):
        atomnameslist = (atomnameslist,)
    chemgroupname = chemgroupdict[resname][groupnumber][1]
    try:
        if resname != 'PRO':
            # rx = mean([x[aid.index(f, start, end)] for f in atomnameslist])
            # ry = mean([y[aid.index(f, start, end)] for f in atomnameslist])
            # rz = mean([z[aid.index(f, start, end)] for f in atomnameslist])
            rx = mean([x[aid.index(f, start, end)] for f in atomnameslist])
            ry = mean([y[aid.index(f, start, end)] for f in atomnameslist])
            rz = mean([z[aid.index(f, start, end)] for f in atomnameslist])
            rbfac = bfac_col(bfacstuff, [bfac[aid.index(f, start, end)] for f in atomnameslist], aidrange, readf_data, chemgroupname)
            outputrecordsstring.append(form(i, readf_data, bfacstuff, chemgroupname, rc, start, rx, ry, rz, rbfac))
            rc = rc + 1
        elif resname == 'PRO':
            rx = mean([x[aid.index(f, start)] for f in ['N', 'C', 'O', 'CA', 'CB', 'CD', 'CG']] + [x[aid.index('N', end)]])
            ry = mean([y[aid.index(f, start)] for f in ['N', 'C', 'O', 'CA', 'CB', 'CD', 'CG']] + [y[aid.index('N', end)]])
            rz = mean([z[aid.index(f, start)] for f in ['N', 'C', 'O', 'CA', 'CB', 'CD', 'CG']] + [z[aid.index('N', end)]])
            rbfac = bfac_col(bfacstuff,
                    [bfac[aid.index(f, start)] for f in ['N', 'C', 'O', 'CA', 'CB', 'CD', 'CG']] + [bfac[aid.index('N', end)]],
                             aidrange, readf_data, chemgroupname)
            outputrecordsstring.append(form(i, readf_data, bfacstuff, chemgroupname, rc, start, rx, ry, rz, rbfac))
            rc = rc + 1
    except Exception as e:
        print('Exception: ' + str(e) + ' while parsing side chain of ' +
              res[start] + chain[start] + resno[start] + ' in ' + str(thepdbfile))
        return rc
        # raise(e)
    return rc

def startingres(i, resname, readf_data, aidrange, outputrecordsstring, bfacstuff, rc):
    start, end = aidrange
    aid = readf_data[1]
    x, y, z, bfac = readf_data[5:-1]
    chmgroup = 'r1'

    # if starting residue is one of the 20 amino acids and has NH3+ group, define it as r5
    if ("N" in aid[end:end+1]) and ("CA" in
        aid[start:end]) and ("C" in aid[start:end]) and ("O" in aid[start:end]):
        if 'N' in aid[start:end]:
            r1x = mean([x[aid.index("N", end, end+1)], x[aid.index("CA", start)],
                        x[aid.index("O", start)], x[aid.index("C", start)],
                        x[aid.index("N", start, end)]])
            r1y = mean([y[aid.index("N", end, end+1)], y[aid.index("CA", start)],
                        y[aid.index("O", start)], y[aid.index("C", start)],
                        y[aid.index("N", start, end)]])
            r1z = mean([z[aid.index("N", end, end+1)], z[aid.index("CA", start)],
                        z[aid.index("O", start)], z[aid.index("C", start)],
                        z[aid.index("N", start, end)]])

            # print(end, i)
            r1bfac = bfac_col(bfacstuff,
                    [bfac[aid.index("N", end, end+1)], bfac[aid.index("CA", start)],
                        bfac[aid.index("O", start)], bfac[aid.index("C", start)],
                        bfac[aid.index("N", start, end)]], [start, end+1], readf_data)
            outputrecordsstring.append(form(i, readf_data, bfacstuff, chmgroup, rc, start, r1x, r1y, r1z, r1bfac))
            rc = rc+1
        else:
            r1x = mean([x[aid.index("N", end, end+1)], x[aid.index("CA", start)],
                        x[aid.index("O", start)], x[aid.index("C", start)]])
            r1y = mean([y[aid.index("N", end, end+1)], y[aid.index("CA", start)],
                        y[aid.index("O", start)], y[aid.index("C", start)]])
            r1z = mean([z[aid.index("N", end, end+1)], z[aid.index("CA", start)],
                        z[aid.index("O", start)], z[aid.index("C", start)]])
            # print(end, i)
            r1bfac = bfac_col(bfacstuff,
                    [bfac[aid.index("N", end, end+1)], bfac[aid.index("CA", start)],
                        bfac[aid.index("O", start)], bfac[aid.index("C", start)]], [start, end+1], readf_data)
            outputrecordsstring.append(form(i, readf_data, bfacstuff, chmgroup, rc, start, r1x, r1y, r1z, r1bfac))
            rc = rc+1
    return rc

def endingres(i, resname, readf_data, aidrange, outputrecordsstring, bfacstuff, rc):
    start, end = aidrange
    aid = readf_data[1]
    resno, x, y, z, bfac, cut = readf_data[4:]
    # if residue is C terminal residue:
        # if residue is Proline:
            # and OXT exists
                # use all residue plus C O and OXT as r11
            # OXT doesn't exist
                # all of the residue is PRO, including C and O, if they exist, defined as r11
        # else if residue is not Proline
            # if OXT exists
            # make C O and OXT into the r1 definition and rest of side chain as defined earlier
            # else
            # use C and O into the r1 definition and rest of side chain as defined earlier
    if (resname == "PRO" and i == len(cut)-2):
        # print('PRO reporting at number', str(resno[i]))
        # print(aid[start:end+1], start, end+1)
        if 'OXT' in aid[start:end+1]:
            if ('N' in aid[start:end]) and ('CA' in aid[start:end]) and ('CB' in
                aid[start:end]) and ('CG' in aid[start:end]) and ('CD' in
                        aid[start:end]) and ('C' in aid[start:]) and ('O' in aid[start:]):
                r11x = (x[aid.index("N", start)]+x[aid.index("CA", start)] +
                        x[aid.index("CD", start)]+x[aid.index("CB", start)] +
                        x[aid.index("CG", start)]+x[aid.index('C', start)] +
                        x[aid.index('O', start)] +x[aid.index('OXT', start)])/8
                r11y = (y[aid.index("N", start)]+y[aid.index("CA", start)] +
                        y[aid.index("CD", start)]+y[aid.index("CB", start)] +
                        y[aid.index("CG", start)]+y[aid.index('C', start)] +
                        y[aid.index('O', start)] +y[aid.index('OXT', start)])/8
                r11z = (z[aid.index("N", start)]+z[aid.index("CA", start)] +
                        z[aid.index("CD", start)]+z[aid.index("CB", start)] +
                        z[aid.index("CG", start)]+z[aid.index('C', start)] +
                        z[aid.index('O', start)] +z[aid.index('OXT', start)])/8
                r11bfac = bfac_col(bfacstuff,
                        [bfac[aid.index("N", start)], bfac[aid.index("CA", start)],
                        bfac[aid.index("CD", start)], bfac[aid.index("CB", start)],
                        bfac[aid.index("CG", start)], bfac[aid.index('C', start)],
                        bfac[aid.index('O', start)], bfac[aid.index('OXT', start)]],
                        [start, end], readf_data, 'r11')
                outputrecordsstring.append(form(i, readf_data, bfacstuff, 'r11', rc, start, r11x, r11y, r11z, r11bfac))
                rc = rc + 1
            else:
                print('Terminal PRO has missing atoms in the ring.')
            # end parsing proline ring atoms
        # end parsing terminal residue
        else:  # if OXT not in terminal but Proline is terminal residue
            if ('N' in aid[start:end]) and ('CA' in aid[start:end]) and ('CB' in
                aid[start:end]) and ('CG' in aid[start:end]) and ('CD' in
                        aid[start:end]) and ('C' in aid[start:]) and ('O' in
                                aid[start:]):
                r11x = (x[aid.index("N", start)]+x[aid.index("CA", start)] +
                        x[aid.index("CD", start)]+x[aid.index("CB", start)] +
                        x[aid.index("CG", start)]+x[aid.index('C', start)] +
                        x[aid.index('O', start)])/7
                r11y = (y[aid.index("N", start)]+y[aid.index("CA", start)] +
                        y[aid.index("CD", start)]+y[aid.index("CB", start)] +
                        y[aid.index("CG", start)]+y[aid.index('C', start)] +
                        y[aid.index('O', start)])/7
                r11z = (z[aid.index("N", start)]+z[aid.index("CA", start)] +
                        z[aid.index("CD", start)]+z[aid.index("CB", start)] +
                        z[aid.index("CG", start)]+z[aid.index('C', start)] +
                        z[aid.index('O', start)])/7
                r11bfac = bfac_col(bfacstuff,
                        [bfac[aid.index("N", start)], bfac[aid.index("CA", start)],
                        bfac[aid.index("CD", start)], bfac[aid.index("CB", start)],
                        bfac[aid.index("CG", start)], bfac[aid.index('C', start)],
                        bfac[aid.index('O', start)]],
                        [start, end], readf_data, 'r11')
                outputrecordsstring.append(form(i, readf_data, bfacstuff, 'r11', rc, start, r11x, r11y, r11z, r11bfac))
                rc = rc + 1
            else:
                print('Terminal PRO has missing atoms in the ring.')
        # end parsing terminal proline residue
    # if terminal residue is not proline
    elif (resname != 'PRO' and i == len(cut)-2 and (resname in NoProList)):
        if 'OXT' in aid[start:end+1]:
            if ('C' in aid[start:end+1]) and ('O' in aid[start:end+1]):
                r1x = mean([x[aid.index(a, start)] for a in ('CA', 'C', 'O', 'OXT')])
                r1y = mean([y[aid.index(a, start)] for a in ('CA', 'C', 'O', 'OXT')])
                r1z = mean([z[aid.index(a, start)] for a in ('CA', 'C', 'O', 'OXT')])
                r1bfac = bfac_col(bfacstuff,
                    [bfac[aid.index(f, start)] for f in ('CA', 'C', 'O', 'OXT')], [start, end+1], readf_data)
                outputrecordsstring.append(form(i, readf_data, bfacstuff, 'r1', rc, start, r1x, r1y, r1z, r1bfac))
                rc = rc + 1
            # end parsing terminal OXT, C and O
            else:
                print('ValueError in ' + thepdbfile + ' : Terminal C or O\
                                 missing for terminal ' + resname + ' ' + str(resno[start]))
        else:
            if ('C' in aid[start:end+1]) and ('O' in
                                              aid[start:end+1]):
                r1x = mean([x[aid.index(a, start)] for a in ('CA', 'C', 'O')])
                r1y = mean([y[aid.index(a, start)] for a in ('CA', 'C', 'O')])
                r1z = mean([z[aid.index(a, start)] for a in ('CA', 'C', 'O')])
                r1bfac = bfac_col(bfacstuff,
                    [bfac[aid.index(f, start)] for f in ('CA', 'C', 'O')], [start, end+1], readf_data)
                outputrecordsstring.append(form(i, readf_data, bfacstuff, 'r1', rc, start, r1x, r1y, r1z, r1bfac))
                rc = rc + 1
            # end parsing terminal OXT, C and O
            else:
                print('ValueError in ' + thepdbfile + ' : CA, C or O missing in terminal ' + resname + ' ' +
                      str(resno[start]))
    return rc

# form is function to write out the output .gpdb file
def form(i, readf_data, bfacstuff, rad, rn, start, rx, ry, rz, rbfac='None'):
    try:
        res, resno, chain = readf_data[2], readf_data[4], readf_data[3]
        bfactoggle = bfacstuff[0]
        if bfactoggle == 'atomids':
            lineout = '{:<6}{:>5} {:<4} {:>3} {:^1}{:>5}   {:>8}{:>8}{:>8}{:>12}'.format(
                    ("ATOM"), (rn), (rad), (res[start]), (chain[start]), (resno[start]), (round(rx, 3)), (round(ry, 3)), (round(rz, 3)),
                str(min(rbfac))+','+str(max(rbfac)))+ '\n'
        elif bfactoggle == 'None':
            lineout = '{:<6}{:>5} {:<4} {:>3} {:^1}{:>5}   {:>8}{:>8}{:>8}'.format(
                    ("ATOM"), (rn), (rad), (res[start]), (chain[start]), (resno[start]), (round(rx, 3)), (round(ry, 3)), (round(rz, 3))) + '\n'
        else:
            lineout = '{:<6}{:>5} {:<4} {:>3} {:^1}{:>5}   {:>8}{:>8}{:>8}      {:>6}'.format(
                ("ATOM"), (rn), (rad), (res[start]), (chain[start]), (resno[start]), (round(rx, 3)), (round(ry, 3)), (round(rz, 3)), round(rbfac, 2)) + '\n'
        return lineout
    except Exception as w:
        print('Exception occurred while writing the output file at ' + str(res[i]) +
              str(resno[i]) + "  " + str(chain[i]) + '. Exception is: ' + str(w))

def gpdbparser(apdbfile, asdir="./", addir="./", bfactoggle=None, atomic_depth=None, dcutoff=7.0):
    '''
    parse pdb file input as apdbfile, located in asdir to gpdb file and write to addir/pdbfilebasename.gpdb
    '''

    # this function reads the pdbfile and returns all the relevant data
    readf_data = ()
    readf_data = thereader(readf_data, pdbfilename=apdbfile, sdir=asdir, ddir=addir)
    ano, aid, res, chain, resno, x, y, z, bfac, cut = readf_data

    bfacdict = {}
    exposedbfac = None
    buriedbfac = None
    if bfactoggle == 'bzs' and atomic_depth is None:
        raise ValueError("'bimodalzscore' calculation requires depth file under -f flag. See help using: gpdbparser.py -h ")
    elif bfactoggle == 'bzs' and atomic_depth is not None:
        print("depth cutoff in Angstrom units:", dcutoff)
        with open(atomic_depth, 'r') as depthfobj:
            depthfile = depthfobj.readlines()
            depthfile = [i for i in depthfile if i.startswith("ATOM")]
            bfacdict = {bfac[i]:float(depthfile[i][60:66].strip()) for i, j in
                           enumerate(depthfile)}
            exposedbfac = [bfac[i] for i, j in
                           enumerate(depthfile) if  float(depthfile[i][60:66].strip()) < dcutoff]
            buriedbfac = [bfac[i] for i, j in enumerate(depthfile)
                          if float(depthfile[i][60:66].strip()) >= dcutoff]
    elif bfactoggle == 'atomids':
        bfac = ano
        readf_data[-2] = ano
    elif bfactoggle not in knownbfac_calculations:
        raise ValueError("BFAC_TOGGLE argument is invalid. Check spelling or see help!")
    bfacstuff = bfactoggle, bfacdict, exposedbfac, buriedbfac, bfac, dcutoff
    # print(buriedbfac, exposedbfac)
    # Main chain parsing:
    # if starting residue and not Proline:
    # define r5 using terminal N
    # if not starting residue and not Proline but one of the other 19 residues:
    # define r1 using CA,C,O,N
    # if ending residue
    # Side Chain parsing:
    # if residue is not Proline, but one of the other 19 amino acids, define side chain chemical
    # groups based on side chain definitions previously defined
    # Proline case:
    # r11 for the whole residue. No extra main chain r1 definition

    outputrecordsstring = []
    rc = 1
    for i in range(0, len(cut)-1):
        start = cut[i]
        end = cut[i+1]
        residue = res[start]

        # STARTING N-TERMINAL RESIDUE MAIN CHAIN PARSING
        if(i == 0) and ((residue in NoProList) or (residue == 'PRO')):
            rc = startingres(i, residue, readf_data, (start, end), outputrecordsstring, bfacstuff, rc)

        # NON-TERMINAL RESIDUES MAIN CHAIN PARSING
        # if any residue not at terminal is NOT PROLINE but one of the 20 residues, define the peptide bond, plus the preceding CA
        # atom, as one group, which is r1
        if(residue != "PRO" and i != len(cut)-2) and i != 0 and (residue in NoProList):
            # print(res[start])
            if ("N" in aid[end:end+1]) and ("CA" in
                                            aid[start:end]) and ("C" in aid[start:end]) and ("O" in
                                                                                             aid[start:end]):
                r1x = mean([x[aid.index("N", end, end+1)], x[aid.index("CA", start)],
                            x[aid.index("O", start)], x[aid.index("C", start)]])
                r1y = mean([y[aid.index("N", end, end+1)], y[aid.index("CA", start)],
                            y[aid.index("O", start)], y[aid.index("C", start)]])
                r1z = mean([z[aid.index("N", end, end+1)], z[aid.index("CA", start)],
                            z[aid.index("O", start)], z[aid.index("C", start)]])
                # print(end, i)
                r1bfac = bfac_col(bfacstuff,
                        [bfac[aid.index("N", end, end+1)], bfac[aid.index("CA", start)],
                            bfac[aid.index("O", start)], bfac[aid.index("C", start)]], [start, end+1], readf_data)
                outputrecordsstring.append(form(i, readf_data, bfacstuff, 'r1', rc, start, r1x, r1y, r1z, r1bfac))
                rc = rc+1
            else:
                print('Main chain atom missing at ' +
                      res[start] + chain[start] + resno[start])

        # ENDING C-TERMINAL RESIDUE PARSING
        if i == len(cut)-2:
            rc = endingres(i, residue, readf_data, (start, end), outputrecordsstring, bfacstuff, rc)

        # SIDECHAIN PARSING
        # define all side chain groups, based on previously defined chemical group definitions of each
        # residue: see chemgroupdict
        if (residue in NoProList and residue != "GLY") or (residue == "PRO" and i != len(cut)-2):
            # sidechain(aid, bfacdict, dcutoff, rc, i, start, end, residue, (x, y, z, aid), readf_data, outputrecordsstring)
            try:
                for groupnumber in sorted(chemgroupdict[residue].keys()):
                    rc = sidechainparser(bfacstuff, rc, i, (start, end), (residue, groupnumber), (x, y, z, aid), readf_data, outputrecordsstring)
            except Exception as e:
                print(str(e))
                # raise(e)
                # pass


    return outputrecordsstring

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    # -i pdbfile -s source_dir -d destination_-b depth
    parser.add_argument("-i", "--input-file", dest='pdbfilename', required=True, help="[REQUIRED]\
                        For example: ./1onc.pdb")
    parser.add_argument("-s", "--source-dir", default='./', dest='sdir', help="Search for pdb file in this\
                        directory. Default is current directory.")
    parser.add_argument("-d", "--destination-dir", default='./', dest='ddir', help="Write out gpdbfile in this\
                        directory. Default is current directory. For example, for pdb file being 1onc.pdb in directory sdir to be\
                        written to ddir destination directory,\
                        program searches for sdir/1onc.pdb and parses it to write out ddir/1onc.gpdb")
    parser.add_argument("-b", "--bfactor-usage", default='None', dest='bfac_toggle',
                        help="Usage of b-factor column. Use 'mean', 'perc' (for percentile), 'zscore', or 'bzs'(for bimodalzscore). Default is None")
    parser.add_argument("-f", "--atomic-depth-file", dest='atomic_depth', help="if\
                        bfactor-usage argument is given as 'bzs' then this file is\
                        required")
    parser.add_argument("-c", "--depthcut", default=7.0, dest='dcutoff', help="if\
                        bfactor-usage argument is given as 'bzs' then this cutoff is\
                        required")
    args = parser.parse_args()
    thepdbfile = getFileNameWithExtension(args.pdbfilename)
    thesdir = args.sdir + '/'
    theddir = args.ddir + '/'
    thebfactoggle = args.bfac_toggle
    outfilename = theddir + getFileNameWithoutExtension(thepdbfile) + ".gpdb"
    outputrecords = gpdbparser(thepdbfile, thesdir, theddir, bfactoggle=thebfactoggle, atomic_depth=args.atomic_depth, dcutoff=float(args.dcutoff))
    
    if len(outputrecords) <= 1:
        # outputrecords doesn't have atom records
        print("WARNING: No ATOMS parsed for " + thepdbfile)
    else:
        with open(outfilename, "w") as result:
            outputrecords.insert(0, "TITLE      GPDB FILE WITH CHEMICAL GROUP DEFINITIONS\n")
            # add all SEQRES statements as header lines, so that later they can be referred to for knowing which residues are terminal residues
            with open(thesdir + "/" + thepdbfile, 'r') as inputpdbfobj:
                inputpdbfile = inputpdbfobj.readlines()
                for inputline in inputpdbfile:
                    if inputline.startswith("SEQRES"):
                        outputrecords.append(inputline)
            for outputline in outputrecords:
                # print(i)
                result.write(outputline)
